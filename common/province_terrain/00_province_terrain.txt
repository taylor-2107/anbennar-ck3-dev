﻿default=jungle

#E_Anbennar

#K_Damescrown

#Damescrown
233=farmlands
234=farmlands
335=plains
914=plains
915=plains

#Aranmas
254=plains
255=plains
262=plains
908=plains

#Tegansfield
258=plains
342=plains
357=plains

#K_Esmaria

#Ryalanar
264=plains
265=plains
575=forest
917=forest

#Cann Esmar
259=plains
260=plains
266=plains
267=plains
911=plains

#Bennonhill
263=mountains
268=farmlands
270=mountains
271=plains
909=plains

#Konwell
269=forest
275=plains
905=forest

#Songbarges
274=farmlands
279=plains
333=plains
916=plains

#Hearthswood
272=forest
273=plains
900=forest

#Low Esmar
44=farmlands
280=farmlands
297=plains
301=hills
576=plains

#High Esmar
302=plains
308=forest
309=plains
901=farmlands
912=hills

#Ashfields
310=plains
311=plains
320=plains
902=plains

#K_Verne

#Eastneck
25=farmlands
45=plains
50=farmlands
51=plains

#Menibor Looop
284=farmlands
285=farmlands
331=farmlands
569=forest

#Galeinn
287=farmlands
295=farmlands
906=plains

#Heroes' Rest
283=plains
286=plains

#Verne
288=forest
289=forest
292=forest
293=plains

#Wyvernmark
290=plains
291=plains
838=hills

#K_Wex

#Wexhills
303=hills
304=hills
305=hills
306=hills

#Ottocam
307=hills
903=forest
907=plains

#Bisan
294=plains
300=plains
315=farmlands
332=plains
579=farmlands
923=plains

#Gnollsgate
316=hills
317=hills
913=plains

#Greater Bardswood
318=forest
319=plains
918=forest

#Rhinmond
415=plains
919=plains

#K_Borders

#Gisden
312=plains
321=plains
326=plains
920=plains

#Arannen
313=forest
325=plains
418=forest
904=farmlands

#Eastborders
322=forest
323=plains
324=plains
421=plains

#Tellum
414=hills
417=hills

#Antir Drop
416=hills
419=hills
420=hills

#Hawkfields
409=plains
410=plains
411=plains

#Highcliff
412=plains
413=forest
921=plains
922=forest

#K_Dameria

#Damesear
1=forest
2=hills	
3=plains
4=mountains
5=forest	
6=plains
8=farmlands
9=hills
11=plains
168=plains
924=farmlands
925=farmlands
926=plains
927=farmlands
928=plains
929=forest
930=plains
931=plains
932=forest
933=forest
934=plains
935=plains
936=plains
938=plains
1041=forest
1042=hills
1043=hills
1044=mountains
1045=hills
1046=hills
1047=plains
1048=forest

524=hills	#Ainathil

#Exwes
18=plains	
153=plains
154=plains
156=plains
939=plains
940=mountains
1062=plains
1063=plains

#Wesdam
10=plains
13=plains
15=plains
17=forest
20=forest
28=plains
31=forest
937=plains
1012=plains
1059=forest
1060=forest
1061=plains

#Neckcliffe
16=plains
21=forest
23=plains
24=plains
26=farmlands
27=plains
941=forest
942=plains
1049=plains
1050=forest

#Acromton
276=hills
330=hills

#Istralore
37=hills
277=hills
899=hills

#Silverwoods
29=forest
52=farmlands
53=forest
54=forest
55=forest
56=forest

#Plumwall
281=plains
328=plains
910=plains

#Heartlands
278=plains
282=plains
296=plains
394=plains

#Middle Luna
109=farmlands
329=plains
587=plains

#Upper Luna
298=farmlands
299=hills
327=farmlands
898=forest

#K_Carneter

#Carneter
19=hills
22=hills
30=forest
57=hills
1010=hills
1011=forest
1051=forest
1057=hills
1058=forest
1148=hills

#K Perlsedge

#Pearlsedge
14=forest
42=forest
43=farmlands
46=plains
943=plains
944=plains
945=plains
1052=forest
1053=forest
1056=hills

#Pearlywine
41=hills
49=hills
946=hills
947=hills
948=hills
949=hills
1054=forest
1055=hills

#K_Tretun

#Tretun
47=plains
48=forest
950=forest
951=hills
1001=plains

#Roilsard
32=hills
33=plains
34=forest
40=plains
59=plains
1002=plains
1003=forest
1004=plains
1005=forest
1006=forest
1007=forest
1008=forest
1009=hills
1094=plains

#E_Lencenor

#K_Rubyhold

#Rubyhold
62=mountains
64=mountains
65=mountains
1230=mountains
1231=mountains
1232=mountains

#K_Sorncost

#Coruan
84=plains
86=hills
89=plains
1076=hills
1077=hills
1078=plains

#Sormanni Hills
91=hills
100=hills
1079=plains
1080=forest
1081=hills
1082=plains
1083=forest

#Sorncost
85=plains
88=plains
90=hills
99=hills
1084=hills
1085=hills
1086=forest
1087=hills
1088=hills
1089=hills
1090=forest
1091=hills

#K_Deranne

#Deranne
110=forest
111=plains
112=plains
113=plains
121=plains
1137=forest
1138=forest
1139=plains
1140=plains
1141=plains
1142=plains
1143=plains
1144=forest
1145=plains
1146=plains
1147=plains

#Darom
58=farmlands
102=forest
104=plains
1126=forest
1127=plains
1128=forest
1129=plains
1130=plains
1131=plains
1132=forest
1133=forest
1134=forest
1135=forest
1136=forest

#K_Lorent

#Lorentaine
67=farmlands
69=mountains
70=plains
1014=plains
1015=forest
1021=forest
1022=forest
1023=forest
1024=mountains
1025=plains
1040=farmlands
1096=plains

#Lorenith
61=forest
68=forest
1013=forest
1016=plains
1018=forest
1064=forest

#Ainethan
38=plains
72=forest
73=plains
1017=hills
1019=plains
1020=forest
1075=hills
1092=plains
1093=hills
1095=forest

#Upper Bloodwine
74=forest
95=forest
108=plains
114=forest
1026=plains
1027=plains
1028=forest
1029=forest
1030=forest
1031=plains
1032=plains
1065=forest
1066=forest
1067=plains
1068=forest
1072=forest

#Lower Bloodwine
79=forest
80=plains
81=plains
97=forest
101=plains
1033=forest
1034=forest
1035=hills
1036=forest
1037=plains
1038=plains
1069=forest
1070=plains
1071=plains
1073=plains
1097=forest

#Enteben
96=plains
98=hills
1039=hills
1074=hills
1106=plains
1107=plains
1108=forest
1109=plains
1110=plains
1111=plains

#Horsegarden
78=plains
105=plains
1118=plains
1119=plains
1120=plains
1121=plains
1122=plains
1123=plains
1124=plains
1125=plains

#Crovania
82=plains
83=farmlands
106=plains
1112=plains
1113=hills
1114=plains
1115=plains
1116=hills
1117=hills

#Venail
93=plains
94=plains
127=plains
1098=forest
1099=forest
1100=forest
1101=forest
1102=forest
1103=plains
1104=forest
1105=plains

#Great Ording
75=plains
92=plains
107=plains
1149=plains
1150=plains
1151=plains
1152=plains
1153=plains
1154=plains
1155=plains
1156=plains
1157=plains

#Rewanwood
77=plains
87=plains
119=plains
1158=plains
1159=plains
1160=plains
1161=plains
1162=plains
1163=forest
1164=plains

#Redglades
115=forest
116=forest
117=forest
118=forest
120=forest
1165=forest
1166=forest
1167=forest
1168=forest
1169=forest
1170=forest
1171=forest
1172=forest

#Rosefield
71=mountains
133=plains
152=mountains
158=plains
1174=plains
1175=forest
1176=plains
1177=plains
1178=plains
1179=plains
1180=farmlands
1181=plains
1182=plains

#E_Small_Country(not in)

#K_Roysfort

#Roysfort
130=plains
131=plains
132=plains
1329=plains
1330=plains
1331=farmlands
1333=forest
1334=plains
1335=plains
1336=plains

#Bigwheat
134=forest
136=plains
1249=plains
1273=plains
1283=forest
1288=forest
1318=plains
1328=plains
1332=forest

#K_Ciderfield

#Appleton
137=plains
139=plains
163=farmlands
1241=plains
1242=forest
1243=forest
1244=plains
1246=farmlands
1247=plains
1248=farmlands

#Pearview
103=plains
147=hills
165=plains
1233=hills
1234=hills
1235=hills
1236=hills
1237=farmlands
1238=plains
1239=hills
1240=hills

#K_Viswall

#Thomsbridge
135=farmlands
159=farmlands
166=plains
1217=farmlands
1218=plains
1219=plains
1220=plains
1221=plains
1222=farmlands
1223=plains
1224=plains
1225=plains
1226=plains
1229=farmlands

#Barrowshire
160=hills
161=hills
162=plains
164=farmlands
1212=hills
1214=hills
1215=hills
1216=hills
1227=plains
1228=plains

#Greymill
12=plains
76=forest
149=plains
1202=forest
1203=plains
1204=forest
1205=forest
1206=forest
1207=plains
1208=farmlands

#Viswall
63=hills
66=farmlands
1213=farmlands

#K_Elkmarch

#Elkmarch
7=forest
206=forest
210=forest
348=forest
1193=forest
1194=forest
1195=forest
1196=forest
1197=forest
1198=forest
1199=forest
1200=forest

#Uelaire
207=plains
208=forest
209=plains
347=plains
1186=forest
1187=plains
1188=plains
1189=plains
1190=plains
1191=plains
1192=plains
1201=plains

#K_Beepeck

#Beepeck
60=wetlands
151=farmlands
155=wetlands
157=forest
205=plains
1183=forest
1184=plains
1185=plains
1209=plains
1210=farmlands
1211=plains

#K_Iochand

#Southroy
123=plains
124=plains
125=plains
128=plains
1245=plains
1337=plains
1338=plains
1339=plains
1340=plains
1341=plains
1430=plains
1432=plains

#Portnamm
126=farmlands

#Iochand
129=plains
141=forest
142=plains
143=plains
1342=plains
1352=plains
1354=plains
1355=plains
1356=plains
1357=plains
1358=plains

#K_Reveria

#Reaver Coast
122=plains
138=plains
140=plains
1359=forest
1363=plains
1368=plains
1390=forest	
1391=plains
1393=plains

#Gnomish Pass
144=plains
145=plains
148=mountains
167=hills
1394=plains
1395=plains
1396=plains
1397=forest
1401=plains
1403=plains
1404=plains
1405=hills
1406=forest

#E_Dragon_Coast

#K_Nimscodd

#Nimscodd
146=hills
169=hills
1398=hills
1399=hills
1400=hills

#Storm Isles
188=hills
190=hills
198=hills
199=hills

#K_Adderodd

#Adderodd
180=hills
182=hills
184=hills

#Royodann
183=mountains
187=hills

#K_Oddansbay

#Norcamb
178=forest
179=hills

#Oddansbay
175=forest
176=plains
177=farmlands

#K_Sildnamm

#Sildnamm
173=hills
174=farmlands

#Widdechand
170=hills
171=hills
172=farmlands
181=farmlands

#K_Mallazex

#Kobildzex
185=mountains
186=mountains
4646=mountains

#Soxun Kobildzex
189=mountains
4643=mountains
4644=mountains
4645=mountains

#E_Alenor

#Coddoran

#Coddoran
197=hills
200=hills
694=plains

#K_Eaglecrest

#Dragonhills
150=mountains
211=hills
213=hills
1407=hills
1409=hills
1410=hills
1412=hills
1420=hills

#Fluddhill
212=hills
214=wetlands
215=wetlands
336=hills
1421=forest
1422=plains
1423=wetlands
1424=wetlands
1425=hills
1426=plains
1427=hills
1428=wetlands
1429=wetlands

#K_Westmoors

#Westmoor
191=hills
192=wetlands
193=wetlands
194=wetlands
345=wetlands
1433=wetlands
1434=hills
1435=hills
1438=wetlands
1439=wetlands
1440=hills
1445=wetlands
1448=wetlands
1449=hills
1450=wetlands
1451=hills
1452=hills
1453=wetlands
1489=hills
1490=wetlands
1491=wetlands
1492=wetlands

#Moorhills
195=hills
196=hills
1446=hills
1447=wetlands
1454=hills
1455=hills
1456=hills

#Beronmoor
201=hills
202=wetlands
203=wetlands
204=wetlands
1457=wetlands
1458=wetlands
1459=wetlands
1460=hills
1461=wetlands
1462=wetlands
1463=wetlands
1464=wetlands
1465=wetlands
1466=plains
1469=wetlands
1470=wetlands
1481=wetlands
1483=wetlands
1484=wetlands
1485=hills
1486=plains
1487=wetlands
1488=wetlands

#K_Adshaw

#Serpentguard
698=hills
699=plains

#Celmaldor
695=plains
696=taiga
697=hills

#Adderwood
700=taiga
703=taiga
724=plains

#Baywic
705=hills
723=hills

#Adshaw
702=taiga
731=taiga
752=taiga

#West_Chillsbay
704=hills
706=taiga
721=plains
722=taiga

#K_Gawed

#Westmounts
217=hills
218=plains
340=hills

#Ginfield
220=forest
337=forest
339=plains

#Vertesk
216=farmlands
219=plains
235=plains
251=plains

#Gawed
221=plains
222=plains
223=taiga
341=hills
346=hills

#Greatmarch
225=plains
226=hills
240=taiga

#Balvord
227=hills
228=taiga
241=taiga

#Alenfield
224=plains
237=plains
239=plains
243=plains
338=plains

#Jonsway
242=taiga
343=plains
344=taiga

#Derwing
236=plains
249=farmlands
253=plains

#Arbaran
238=forest
244=forest
252=forest
349=forest

#Golden_Plains
250=plains
334=plains

#Cestir
245=forest
246=forest
247=forest
256=forest

#E_Businor

#K_Busilar

#Khenak
375=mountains
376=hills
377=mountains

#Lorinhap
374=hills
378=plains

#Busilari Straits
39=plains
372=plains
373=plains

#Lioncost
365=hills
367=hills
368=plains

#Hapiande
370=plains
371=hills

#Lorbet
366=hills
408=plains

#Mountainway
362=plains
363=forest
364=mountains

#K_Eborthil

#Eborthil
369=hills
380=hills

#Tefkora
35=hills
36=hills
379=hills

#E_Castanor

#K_Ibevar 

#Ibevar
261=hills
350=mountains
351=forest
352=forest
359=forest
360=forest

#Varillen
353=forest
354=hills
356=hills
361=forest

#Rotwall
314=plains
422=mountains
423=hills

#Cursewood
248=forest
257=forest
355=forest
358=forest

#Whistlevale
775=plains
777=hills
778=hills
779=hills

#K_Bal_Mire

#Middle_Alen
710=hills
725=taiga
726=taiga

#Bal_Mire
229=wetlands
230=wetlands
231=wetlands
232=wetlands

#False_Mire
759=hills
761=hills
762=plains

#K_Cannwood

#Cannwood
727=hills
728=forest
750=hills

#Approach
751=hills
753=taiga
758=hills

#K_Vrorenmarch

#East_Chillsbay
701=taiga
707=plains
709=plains
712=plains
713=taiga

#Sondar
714=taiga
715=taiga

#Cedesck
717=plains
718=plains
719=taiga
720=hills

#Vrorenmarch
735=plains
736=taiga
740=taiga

#Wudhall
711=taiga
716=taiga

#Gulletpeak
733=mountains
734=mountains

#Vrorenwall
737=taiga
738=taiga
739=hills

#Athfork
729=taiga
730=taiga
732=mountains
749=taiga

#Ebonmarck
741=taiga
742=hills
744=taiga
745=taiga
748=taiga

#K_Adenica

#Adenica
763=hills
764=hills
765=hills
792=farmlands

#Silvervord
756=farmlands
757=farmlands
760=hills
767=hills

#Rohibon
768=plains
769=plains
770=plains
771=plains

#Taran
772=plains
773=plains
774=forest

#Valefort
776=mountains
780=mountains

#Verteben
781=hills
782=hills

#K_Merewood

#Northmerewood
766=forest
783=forest
788=forest
790=forest

#Merescker
784=plains
785=forest
786=forest
787=forest
789=forest

#Oudmerewood
793=plains
794=forest
795=hills
796=forest

#K_Devaced

#Devaced
791=forest
797=forest
798=farmlands
799=plains

#Oudeben
800=forest
801=plains
808=forest
809=plains
810=forest

#Dostans_Way
802=forest
803=forest
804=forest
805=forest
807=forest

#K_Cast

#Northmere
755=plains
834=farmlands
835=farmlands
836=plains

#Ernicknath
837=farmlands
839=farmlands

#Westgate
747=taiga
754=taiga
842=forest
848=hills #taiga

#Northyl
743=hills
746=taiga
849=hills
850=hills

#Trialmount
840=mountains
843=plains
845=taiga
847=mountains

#Serpentsmarck
846=taiga
852=mountains
858=mountains
859=taiga

#K_Castonath

#Castonath
831=farmlands
832=farmlands
833=farmlands

#K_Anor

#Southgate
828=hills #forest
829=farmlands
830=farmlands

#Upenath
841=plains
853=forest
855=forest

#Oldhaven
866=forest
873=forest
874=hills

#Bradsecker
844=hills
851=plains
854=hills

#Ardent_Glade
864=hills
865=hills
869=hills

#Steelhyl
860=mountains
861=mountains
862=hills #less steep

#K_Sarwick

#Clouded_Wood
863=mountains
868=mountains
870=hills #taiga

#Sarwood
872=hills
878=forest
879=forest

#Nortessord
867=hills
871=mountains
881=forest

#Esshyl
880=hills
882=mountains
884=mountains

#K_Humacfeld

#Burnoll
821=farmlands
823=hills
825=hills

#Themin
820=hills
822=hills

#Humacvord
824=hills
826=hills
827=farmlands
856=farmlands
857=plains

#K_Blademarches

#Clovenwood
806=forest
811=forest
815=forest

#Beastgrave
812=forest
813=forest
814=hills

#Blademarch
816=farmlands
817=hills
818=farmlands
819=farmlands

#Medirleigh
875=hills
876=farmlands
877=hills
885=farmlands

#Swapstroke
886=plains
887=plains

#K_Marrhold

#Dryadsdale
883=hills
888=forest
891=forest

#Marrhold
895=mountains
896=mountains
897=mountains
4097=mountains

#Hornwood
889=plains
890=plains
892=hills

#Doewood
893=hills
894=mountains

#E_Dostanor

#K_Corvuria

#Tiferben
424=mountains
425=hills
426=plains
427=plains
428=plains
430=plains

#Blackwoods
429=forest
435=forest
436=forest
437=hills

#Ravenhill
431=hills
432=plains
433=forest
434=forest

#Bal_Dostan
438=hills
439=forest
440=forest
441=hills #Arca Corvur

#K_Corveld

#Dreadmire
442=wetlands
443=wetlands
444=wetlands
446=wetlands
450=wetlands

#Corveld
445=wetlands
447=wetlands
448=wetlands
449=wetlands
451=wetlands

#K_Ourdia

#Lencmark
506=plains
507=forest
508=forest

#Tencmark
509=forest
510=hills
511=hills

#Ourdmark
512=hills
513=mountains
514=hills
515=hills

#E_Gerudia

#K_Jotuntar

#Norjotuntar
987=taiga
988=taiga
989=taiga

#Sudjotuntar
990=taiga
991=taiga
992=taiga

#K_Urviksten

#Esfjall
984=mountains
986=mountains

#Naugsvol
981=mountains
982=taiga
983=mountains

#Avnkaup
978=wetlands
979=mountains
980=taiga

#K_Rimurhals

#Jotunhamr
985=hills

#Olavslund
975=taiga
976=mountains
977=taiga

#Drekkiskali
972=mountains
973=mountains
974=mountains

#K_Bjarnik

#Bifrutja
708=hills
952=hills
953=hills

#Bjarnland
954=mountains
955=plains
956=plains

#Alptborg
957=taiga
964=mountains

#Siadett
965=taiga
966=wetlands
967=taiga

#Ismark
968=taiga
970=taiga
971=taiga

#Revrland
959=plains
960=plains
993=plains
997=plains

#Haugmyrr
961=taiga
962=taiga

#Sarbann
958=taiga
963=taiga

#Kaldrland
969=taiga
998=taiga

#K_Obrtroll

#Dalrfjall
994=mountains
995=mountains
1000=mountains

#Thurrsbol
996=mountains
999=hills

#E_Kheterata

#K_Ehka

#Ekha
386=drylands
387=drylands
388=hills
402=drylands
407=hills

#Akarat
389=hills
390=desert_mountains
391=desert_mountains
392=desert_mountains
393=desert_mountains

#K_Khasa

#Khasa
383=hills
384=hills
385=hills
400=drylands
406=hills

#Ikasakan
395=desert_mountains
396=desert_mountains
398=desert_mountains
401=desert_mountains

#K_Deshak

#Deshak
381=hills
382=drylands
403=drylands
404=drylands
405=hills

#Hapak
397=desert_mountains
399=desert_mountains
453=desert
458=desert

#K_Ayshanaz

#Krahkeysa
6091=desert
6092=desert
6093=desert
6094=desert
6095=desert

#Axakmoz
6096=oasis
6097=desert
6098=desert
6099=desert

#Boozazn
459=desert
460=desert
461=desert

#K_Kheterata

#Ohitsopot
454=desert
455=desert
456=desert
457=desert

#Sopotremit
462=floodplains
463=floodplains
503=floodplains
546=floodplains

#Aakheta
464=floodplains
466=floodplains
467=floodplains
468=floodplains

#Anarat
469=floodplains
471=floodplains
6088=floodplains

#Nirat
472=desert
473=floodplains
491=desert

#Khetarat
474=floodplains
475=floodplains
502=desert
592=desert
6089=desert

#Golkora 
476=desert
477=desert
478=desert
479=floodplains

#Hittputiushesh
465=desert
470=desert
490=desert

#Ibtat Axast
492=desert
493=desert
495=desert
497=desert

#Awaashesh
504=desert
505=desert

#Masusopot
498=desert
499=desert
500=desert
501=desert

#Ibtatu
494=drylands
496=drylands

#K_Irsmahap

#Hapmot
480=desert
481=desert
482=desert
6090=desert

#Dawimshesh
483=drylands
484=drylands
486=drylands
487=drylands
5676=drylands

#Miugesh
5478=drylands
5479=drylands
5480=drylands

#E_Bulwar

#K_Re_Uyel

#Crathanor
452=hills
516=hills
517=mountains
518=mountains
519=mountains

#Re_Uyel
521=hills
522=hills
523=hills
581=hills

#Medbahar
520=hills
528=forest
529=mountains

#K_Ovdal_Tungr

#Ovdal_Tungr
525=mountains
526=mountains
527=mountains

#K_Bahar

#Magairous
530=mountains
531=forest
534=mountains
535=forest

#Aqatbar
532=desert_mountains
536=forest
537=forest
541=forest
544=desert_mountains

#Azka_Evran
538=forest
539=desert_mountains
540=desert_mountains

#Azkasad
649=desert_mountains
650=desert_mountains

#Birsartenslib
542=hills
543=forest
549=forest

#Bahar Szel-Uak
545=desert_mountains
547=forest
548=forest
550=forest

#K_Gelkalis

#Eludasul
652=desert_mountains
653=hills
654=hills

#Gelkarzan
655=desert_mountains
656=drylands

#Gelkalis
664=hills
665=desert_mountains

#K_Firanyalen

#Hranapas
668=desert_mountains
669=desert_mountains
670=desert
671=desert_mountains
4117=desert_mountains

#Uvolate
672=hills
673=hills
674=hills

#Firanyalen
675=hills
676=hills
677=mountains
678=mountains

#K_Harpylen

#Creylore
666=desert_mountains
667=desert_mountains

#Harpylen
660=desert_mountains
661=desert_mountains
663=desert_mountains

#Alyzksaan
651=desert_mountains
657=desert_mountains
658=desert_mountains
659=desert_mountains
662=desert_mountains

#K_Imuluses
551=desert_mountains
552=desert_mountains
553=desert_mountains
554=desert_mountains
555=desert_mountains
556=desert_mountains
557=desert_mountains

#K_Idanas
559=drylands
560=floodplains
586=floodplains
588=floodplains
593=drylands
594=drylands
595=drylands

#K_Brasan
561=floodplains
562=drylands
563=floodplains
564=floodplains
565=floodplains
567=floodplains
568=drylands
570=desert
572=floodplains
584=desert_mountains

#K_Drolas
573=hills
574=hills
577=hills
578=hills
580=hills
582=hills

#K_Sad_Sur

679=desert_mountains
680=desert_mountains
682=desert_mountains

597=desert_mountains
683=desert_mountains
685=desert_mountains

686=desert_mountains
692=desert_mountains
693=desert_mountains

688=desert_mountains
689=desert_mountains

687=desert_mountains
691=desert_mountains

#K_Kumarses
589=desert_mountains
590=drylands
591=floodplains
603=drylands
608=floodplains
609=drylands
610=drylands
614=drylands

#K_Bulwar
596=floodplains
598=floodplains
599=floodplains
600=floodplains	
601=floodplains
602=floodplains
604=floodplains
605=desert
606=desert
607=floodplains
611=drylands
612=floodplains
625=floodplains
626=floodplains
627=floodplains
690=desert

#Akalzes
613=floodplains
615=drylands
616=floodplains
617=desert_mountains
618=desert_mountains
619=desert_mountains
620=drylands
621=drylands
622=drylands
623=drylands
624=drylands
628=drylands

#avamezan
629=drylands
630=drylands
632=drylands
633=drylands

#Hasr
631=drylands
634=drylands
635=drylands
636=drylands
637=drylands
638=desert
639=drylands
640=drylands
641=drylands

#Azka-Sur
558=desert_mountains
585=desert_mountains
642=drylands
643=drylands
644=desert
645=desert_mountains
646=desert_mountains
647=desert_mountains
648=desert

#Seghdihr
4124=desert_mountains

#E_Salahad

#Harragrar
485=desert
488=desert
489=desert
571=desert
583=desert
681=desert
684=desert
6100=desert
6101=desert
6102=desert
6103=desert
6104=desert

#Krahwix
5461=desert
5462=drylands
5463=drylands
5464=drylands
5465=desert
5472=drylands
5475=desert
5476=drylands
5477=drylands
5532=drylands
5533=desert

#Dasmazar
566=desert_mountains
5431=desert
5432=desert
5433=desert
5434=desert
5435=desert
5435=desert
5667=desert
5668=desert

#Apasih
2910=desert_mountains
2911=desert
2912=desert
2913=desert_mountains
2923=desert
2924=desert
2925=desert
2926=desert
2927=desert
2928=desert
2929=desert
2930=desert
4400=desert

#Siadanlen
2900=desert
2901=desert
2902=desert
2903=desert_mountains
2904=desert_mountains
2905=desert
2906=desert_mountains
2907=desert
2908=desert_mountains
2909=desert_mountains
2914=desert_mountains
2915=desert
2916=desert
2917=desert_mountains
2918=desert_mountains
2919=desert
2920=desert
2921=desert_mountains
2922=desert_mountains
4376=desert_mountains