template = {


	#################################################
	################### Anbennar ####################
	#################################################


	basic_bulwar_01 = {
		pattern = list "basic_division"
		color1 = list "normal_colors"
		color2 = list "normal_colors"
		color3 = list "fire_colors"
		color4 = list "fire_colors"
		colored_emblem = {
			texture = list "bulwari_charge_list"
			color1 = color3
			color2 = color4
			color3 = "damerian_white"
			instance = { position = { 0.5 0.5 }  scale = { 0.9 0.9 }  }			
		}
	}

	chain_bulwar_01 = {
		pattern = list "basic_division"
		color1 = list "normal_colors"
		color2 = list "normal_colors"
		color3 = list "fire_colors"
		color4 = list "fire_colors"
		colored_emblem = {
			texture = list "bulwari_charge_list"
			color1 = color3
			color2 = color4
			color3 = "damerian_white"
			instance = { position = { 0.5 0.5 }  scale = { 0.9 0.9 }  }			
		}
		colored_emblem = {
			texture = "ce_anb_chain.dds"
			color1 = "damerian_black"
			color2 = "damerian_black"
			instance = { position = { 0.5 0.5 }	scale = { -1.0 1.0 }	}
		}
	}

	chain_bulwar_01b = {
		pattern = list "basic_division"
		color1 = list "normal_colors"
		color2 = list "normal_colors"
		color3 = list "fire_colors"
		color4 = list "fire_colors"
		colored_emblem = {
			texture = "ce_anb_chain.dds"
			color1 = "damerian_black"
			color2 = "damerian_black"
			instance = { position = { 0.5 0.5 }	scale = { 1.0 1.0 }	}
		}
		colored_emblem = {
			texture = list "bulwari_charge_list"
			color1 = color3
			color2 = color4
			color3 = "damerian_white"
			instance = { position = { 0.5 0.5 }  scale = { 0.9 0.9 }  }			
		}
	}

	chain_bulwar_02 = {
		pattern = list "basic_division"
		color1 = list "normal_colors"
		color2 = list "normal_colors"
		color3 = list "fire_colors"
		color4 = list "fire_colors"
		colored_emblem = {
			texture = "ce_anb_chain.dds"
			color1 = "damerian_black"
			color2 = "damerian_black"
			instance = { position = { 0.5 0.5 }	scale = { -1.0 1.0 }	}
		}
		colored_emblem = {
			texture = list "bulwari_charge_list"
			color1 = color3
			color2 = color4
			color3 = "damerian_white"
			instance = { position = { 0.5 0.5 }  scale = { 0.9 0.9 }  }			
		}
		colored_emblem = {
			texture = "ce_anb_chain.dds"
			color1 = "damerian_black"
			color2 = "damerian_black"
			instance = { position = { 0.5 0.5 }	scale = { 1.0 1.0 }	}
		}
	}

	basic_bulwar_framed_01 = {
		pattern = "pattern_solid.dds"
		color1 = list "normal_colors"
		color2 = list "normal_colors"
		color3 = list "fire_colors"
		color4 = list "fire_colors"
		colored_emblem = {
			texture = list "african_circle_frame_list"
			color1 = list "normal_colors"
			color2 = list "normal_colors"
			instance = { position = { 0.5 0.52 }  scale = { 1.1 1.1 }  }			
		}
		colored_emblem = {
			texture = list "bulwari_circle_charge_list"
			color1 = color3
			color2 = color4
			color3 = "damerian_white"
			instance = { position = { 0.5 0.5 }  scale = { 0.9 0.9 }  }			
		}
	}

	basic_bulwar_framed_02 = {
		pattern = "pattern_solid.dds"
		color1 = list "normal_colors"
		color2 = list "normal_colors"
		color3 = list "fire_colors"
		color4 = list "fire_colors"
		colored_emblem = {
			texture = list "african_circle_frame_bg_list"
			color1 = color2
			color2 = color2
			color3 = "damerian_white"
			instance = { position = { 0.5 0.52 }  scale = { 1.1 1.1 }  }			
		}
		colored_emblem = {
			texture = list "bulwari_circle_charge_list"
			color1 = color3
			color2 = color4
			color3 = "damerian_white"
			instance = { position = { 0.5 0.5 }  scale = { 0.9 0.9 }  }			
		}
	}

	basic_bulwar_framed_03 = {
		pattern = "pattern_solid.dds"
		color1 = list "normal_colors"
		color2 = list "normal_colors"
		color3 = list "fire_colors"
		color4 = list "fire_colors"
		colored_emblem = {
			texture = list "african_circle_frame_bg_list"
			color1 = color2
			color2 = color2
			color3 = "damerian_white"
			instance = { position = { 0.5 0.52 }  scale = { 1.1 1.1 }  }			
		}
		colored_emblem = {
			texture = list "african_circle_frame_list"
			color1 = color3
			color2 = color4
			color3 = "damerian_white"
			instance = { position = { 0.5 0.52 }  scale = { 1.1 1.1 }  }			
		}
		colored_emblem = {
			texture = list "bulwari_circle_charge_list"
			color1 = color3
			color2 = color4
			color3 = "damerian_white"
			instance = { position = { 0.5 0.5 }  scale = { 0.9 0.9 }  }			
		}
	}

	###taken from vanilla templates but changed the emblem list

	roundel_bulwar_template = {
		pattern = "pattern_solid.dds"
		color1 = list "normal_colors"
		color2 = list "normal_colors"
		color3 = list "metal_colors"

		colored_emblem = {
			texture = list "mena_roundel_frame_list"
			color1 = color3
			color2 = list "metal_colors"
			instance = { position = { 0.5 0.48 } scale = { 0.75 0.75 }  }		
		}				
		colored_emblem = {
			texture = list "bulwari_circle_charge_list"
			color1 = color1
			color2 = color1
			instance = { position = { 0.5 0.48 } scale = { 0.7 0.7 }  }			
		}
	}	
	roundel_bulwar_template_inverted = {
		pattern = "pattern_solid.dds"
		color1 = list "metal_colors"
		color2 = list "metal_colors"
		color3 = list "normal_colors"

		colored_emblem = {
			texture = list "mena_roundel_frame_list"
			color1 = color3
			color2 = list "normal_colors"
			instance = { position = { 0.5 0.48 } scale = { 0.75 0.75 }  }		
		}				
		colored_emblem = {
			texture = list "bulwari_circle_charge_list"
			color1 = color1
			color2 = color1
			instance = { position = { 0.5 0.48 } scale = { 0.7 0.7 }  }			
		}
	}	
}