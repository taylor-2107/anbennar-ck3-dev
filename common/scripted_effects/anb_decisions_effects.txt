﻿#Anbennar - cleared these

#Anbennar
special_succession_elven_effect = {
	if = {
		limit = { highest_held_title_tier = tier_empire }
		every_held_title = {
			limit = {
				tier = tier_empire
				NOR = {
					has_title_law_flag = advanced_succession_law
					has_title_law_flag = elective_succession_law
				}
			}
			add_title_law = elven_elective_succession_law
		}
		every_vassal = {
			limit = { has_culture_group = culture_group:elvish_group }
			add_opinion = {
				modifier = implemented_traditional_succession_law_opinion
				years = 20
				target = root
			}
			custom = major_decisions.3100.tt_opinion_elven
		}
	}
	else = {
		every_held_title = {
			limit = {
				tier = tier_kingdom
				NOR = {
					has_title_law_flag = advanced_succession_law
					has_title_law_flag = elective_succession_law
				}
			}
			add_title_law = elven_elective_succession_law
		}
		every_vassal = {
			limit = { has_culture_group = culture_group:elvish_group }
			add_opinion = {
				modifier = implemented_traditional_succession_law_opinion
				years = 20
				target = root
			}
			custom = major_decisions.3100.tt_opinion_elven
		}
	}
}
