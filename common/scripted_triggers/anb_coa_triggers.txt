﻿#Triggers to control Coat of Arms weighting

coa_verne_dameria_trigger = {
	OR = {
		primary_title = {
			this = title:k_dameria
		}
		any_liege_or_above = {
			primary_title = {
				this = title:k_dameria
			}
		}
	}
}

coa_damerian_trigger = {
	culture = culture:damerian
}

coa_old_damerian_trigger = {
	culture = culture:old_damerian
}

coa_vernman_trigger = {
	OR = {
		culture = culture:vernman
		culture = culture:vernid
	}
}

coa_gawedi_trigger = {
	culture = culture:gawedi
}

coa_sun_elvish_trigger = {
	culture = culture:sun_elvish
}

coa_gnomish_trigger = {
	OR = {
		culture = culture:cliff_gnomish
		culture = culture:creek_gnomish
		culture = culture:imperial_gnomish
	}
}

coa_bulwari_trigger = {
	OR = {
		culture = culture:zanite
		culture = culture:brasanni
		culture = culture:gelkar
		culture = culture:bahari
		culture = culture:surani
		culture = culture:sadnatu
		culture = culture:masnsih
		culture = culture:maqeti
	}
}
