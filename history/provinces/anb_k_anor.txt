862 = { #Coalwoud

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
	1020.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}

860 = { #Steelhyl

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
	1020.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}

861 = { #Alloysford

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

869 = { #Eswall

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
	1020.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}

864 = { #Cadells_Rest

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

865 = { #Ardent_Keep

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
	1020.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}

844 = { #Mintirley

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

851 = { #Marronath

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

854 = { #Bradnath

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

841 = { #Escerton

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

853 = { #Foarhal

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

855 = { #Nathwoud

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
	1020.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}

828 = { #Ar_Urion

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
	1020.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}

829 = { #Silverdocks

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
	1020.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}

830 = { #Southgate

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
	1020.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}

873 = { #Hagstow

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
	1020.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}

874 = { #Oldhaven

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
	1020.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}

866 = { #Annistoft

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
	1020.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}