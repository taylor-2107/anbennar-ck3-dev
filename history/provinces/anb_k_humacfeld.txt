821 = { #Camircost

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

825 = { #Smallmere

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

823 = { #Burnoll

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

856 = { #Forkwic

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

857 = { #Grannvale

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

826 = { #Wystanway

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

824 = { #Humacs_Rest

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

827 = { #Ionntras

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
	1020.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}

820 = { #Theminath

	# Misc
	culture = marcher
	religion = castanorian_pantheon

	# History
}

822 = { #Haresleigh

	# Misc
	culture = marcher
	religion = castanorian_pantheon

	# History
}