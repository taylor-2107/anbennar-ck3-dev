2 = {	# 1 - 1
	
	# Misc
	culture = damerian
	religion = cult_of_the_dame
	holding = castle_holding
	# History
}

3 = {	# 1 - 1
	
	# Misc
	culture = damerian
    religion = cult_of_the_dame
    holding = city_holding
	
	# History
}

4 = {	# 1 - 1
	
	# Misc
	culture = damerian
	religion = cult_of_the_dame
    holding = church_holding
    
    # History
}

5 = {	# 1 - 1
	
	# Misc
	culture = moon_elvish
	religion = cult_of_the_dame
	holding = city_holding
	# History
}

6 = {	# 1 - 1
	
	# Misc
	culture = damerian
	religion = cult_of_the_dame
	holding = castle_holding
	# History
}

8 = {	# 1 - 1
	
	# Misc
	culture = damerian
    religion = cult_of_the_dame

    holding = castle_holding
	
	# History
}

168 = {

    # Misc
    culture = damerian
    religion = cult_of_the_dame

    holding = castle_holding

    # History
}

#County of Exwes
153 = { #Exwes

    # Misc
    culture = exwesser
    religion = cult_of_the_dame
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History

}

#County of Exwes by the Sea
154 = { #Exwes by the Sea

    # Misc
    culture = exwesser
    religion = cult_of_the_dame
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History

}

#County of Hookfield
156 = { #Hookfield

    # Misc
    culture = exwesser
    religion = cult_of_the_dame
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History

}

#County of Gabelaire
15 = { #Gabelaire

    # Misc
    culture = lenco_damerian
    religion = cult_of_the_dame
    holding = city_holding
    #terrain = (doesn't seem to work at all)

    # History

}

17 = { #Lenceiande

    # Misc
    culture = lenco_damerian
    religion = cult_of_the_dame
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History

}

#County of Wesdam
10 = { #Wesdam

    # Misc
    culture = damerian
    religion = cult_of_the_dame
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

11 = {	# 1 - 1
	
	# Misc
	culture = damerian
	religion = cult_of_the_dame
	holding = castle_holding
	# History
}

13 = { #Toothsend

    # Misc
    culture = damerian
    religion = cult_of_the_dame
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History

}

15 = {	# 1 - 1
	
	# Misc
	culture = damerian
	religion = cult_of_the_dame
	
	# History
}

17 = {	# 1 - 1
	
	# Misc
	culture = damerian
	religion = cult_of_the_dame
	
	# History
}

#County of Ilvandet
31 = { #Ilvandet

    # Misc
    culture = damerian
    religion = cult_of_the_dame
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History

}

20 = { #Ordoin

    # Misc
    culture = lenco_damerian
    religion = cult_of_the_dame
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History

}

#County of Neckcliffe
27 = { #Neckcliffe

    # Misc
    culture = lenco_damerian
    religion = cult_of_the_dame
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History

}

24 = { #Stoneview

    # Misc
    culture = lenco_damerian
    religion = cult_of_the_dame
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History

}

23 = { #Triancost

    # Misc
    culture = lenco_damerian
    religion = cult_of_the_dame
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History

}

26 = { #Throatport

    # Misc
    culture = lenco_damerian
    religion = cult_of_the_dame
    holding = city_holding
    #terrain = (doesn't seem to work at all)

    # History

}

#County of Dockbridge
21 = { #Dockbridge

    # Misc
    culture = lenco_damerian
    religion = cult_of_the_dame
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History

}

#County of Windtower
16 = { #Windtower

    # Misc
    culture = lenco_damerian
    religion = cult_of_the_dame
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History

}

52 = {

    # Misc
    culture = moon_elvish
    religion = cult_of_the_dame

    # History

}

53 = {

    # Misc
    culture = moon_elvish
    religion = cult_of_the_dame

    # History

}

54 = {

    # Misc
    culture = moon_elvish
    religion = cult_of_the_dame

    # History

}

55 = {

    # Misc
    culture = moon_elvish
    religion = cult_of_the_dame

    # History

}

56 = {

    # Misc
    culture = moon_elvish
    religion = cult_of_the_dame

    # History

}

29 = {

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame

    # History

}

37 = {

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame

    # History

}

276 = {

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame

    # History
}

277 = {

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame

    # History
	1020.1.1 = {
		special_building_slot = damerian_dales_mines_01
		special_building = damerian_dales_mines_01
	}
}

330 = {

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame

    # History

}

899 = {

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame

    # History

}

281 = {

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame

    # History

}

328 = {

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame
	holding = city_holding

    # History
	1020.1.1 = {
		special_building_slot = silvelar_mines_01
		special_building = silvelar_mines_01
	}

}

910 = {

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame

    # History

}

278 = {

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame

    # History

}

282 = {

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame

    # History

}

296 = {

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame

    # History

}

394 = {

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame

    # History

}

109 = {

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame

    # History

}

329 = {

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame

    # History

}

587 = {

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame

    # History

}

298 = {

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame

    # History

}

299 = {

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame

    # History

}

327 = {

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame

    # History

}

898 = {

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame

    # History

}