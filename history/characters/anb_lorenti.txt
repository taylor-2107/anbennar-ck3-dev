﻿20000 = { #Thayen - Duke of Lower Bloodwine
	name = "Talan"
	dna = 20000_duke_thayen
	dynasty = dynasty_aubergentis
	religion = house_of_minara
	culture = lorenti

	diplomacy = 3
	martial = 7
	stewardship = 3
	intrigue = 5
	learning = 4
	prowess = 8

	trait = arrogant
	trait = brave
	trait = wrathful
	trait = education_martial_3
	trait = physique_good_1
	trait = scarred
	trait = irritable
	trait = open_terrain_expert
	
	978.12.4 = {
		birth = yes
	}

	999.9.29 = {
		add_spouse = 20003
	}

	1020.2.6 = {
		add_spouse = 20004
		effect = {
			add_trait = human_purist
			set_variable = {
				name = racial_attitude_set
				value = yes
			}
		}
	}
}

20001 = { #Thayen's firstborn
	name = "Calas"
	dna = 20001_calas_aubergentis
	dynasty = dynasty_aubergentis
	religion = house_of_minara
	culture = lorentish
	father = 20000
	mother = 20003

	diplomacy = 5
	martial = 1
	stewardship = 4
	intrigue = 2
	learning = 8
	prowess = 0

	trait = lustful
	trait = content
	trait = trusting
	trait = education_learning_3
	trait = physique_bad_1
	trait = scarred

	1005.5.20 = {
		birth = yes
	}

	1012.5.10 = {
		effect={
			set_relation_guardian = character:20005
			set_relation_friend = character:20005
			reverse_add_opinion = {
				modifier = disappointed_opinion
				opinion = -100
				target = character:20000
			}
		}
	}

	1021.5.20 = {
		effect={
			remove_relation_guardian = character:20005
		}
	}
}

20002 = {  #Thayen's secondborn
	name = "Riualtr"
	dna = 20002_adran_aubergentis
	dynasty = dynasty_aubergentis
	religion = house_of_minara
	culture = lorenti
	father = 20000
	mother = 20004

	diplomacy = 4
	martial = 4
	stewardship = 4
	intrigue = 4
	learning = 4
	prowess = 4

	trait = physique_good_1
	
	1020.12.4 = {
		birth = yes
	}
}

20003 = {  #Thayen's (dead) wife
	name = "Elwen"
	dna = 20003_aria_aubergentis
	dynasty = dynasty_aubergentis
	religion = house_of_minara
	culture = lorenti
	female = yes
	
	985.1.7 = {
		birth = yes
	}

	1005.5.20 = { #Died giving birth to Calas
		death = {
			death_reason = death_childbirth
		}
	}
}

20004 = {  #Thayen's (gamestart) wife
	name = "Susanne"
	dna = 20004_isobel_aubergentis
	dynasty = dynasty_aubergentis
	religion = house_of_minara
	culture = lorenti
	female = yes
	
	trait = temperate
	trait = impatient
	trait = ambitious

	993.1.7 = {
		birth = yes
	}
}

20005 = {  #Cala's elven guardian while Thayen was away at war
	name = "Ivran"
	dna = 20005_ivran
	religion = elven_forebears
	culture = moon_elvish
	
	trait = temperate
	trait = trusting
	trait = lazy

	917.1.7 = {
		birth = yes
		effect = {
			set_to_lowborn = yes
		}
	}

	1012.5.10 = {
		employer = 20000
	}
	
	1019.8.15 = {
		effect = {
			reverse_add_opinion = {
				modifier = ruined_my_heir_opinion
				target = character:20000
			}

			reverse_add_opinion = {
				modifier = mentored_me_opinion
				target = character:20001
			}
		}
	}
}
