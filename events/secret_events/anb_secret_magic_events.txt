### Forbidden Magic ###

namespace = anb_secret_magic

anb_secret_magic.1 = {
	type = character_event
	title = anb_secret_magic.1.t
	desc = anb_secret_magic.1.desc
	theme = secret
	right_portrait = scope:secret_exposer

	immediate = {
		secret_exposed_owner_effects_effect = { SECRET = scope:secret POV = root }
		secret_exposed_owner_opinion_effects_effect = yes
		play_music_cue = "mx_cue_stress"
	}

	option = {
		name = anb_secret_magic.1.a
	}
}